<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="model.Empresa" %>
        <!DOCTYPE html>
        <html lang="pt-br">

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Visualizar Empresa</title>

        </head>

        <body>
                       <!-- Modal -->
            <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="modalLabel">Excluir Empresa</h4>
                        </div>
                        <div class="modal-body">
                            Deseja realmente excluir esta empresa?
                        </div>
                        <div class="modal-footer">
                            <form action="ManterEmpresa.do" method="post">
                                <input type="hidden" name="id" id="id_excluir" />
                                <button type="submit" class="btn btn-primary" name="acao" value="Excluir">Sim</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal -->
                <!-- Barra superior com os menus de navegação -->
				<c:import url="header-empresa.jsp"/>
                <!-- Container Principal -->
                <div id="main" class="container">
	                <h3 class="page-header">Visualizar Empresa "${empresa.nome}"</h3>
			            <div class="row">
			                <div class="col-md-12">
			                    <p><strong>CNPJ</strong>
			                    </p>
			                    <p>
			                        ${empresa.cnpj}
			                    </p>
			                </div>
			            </div>
			             <div class="row">
			                <div class="col-md-6">
			                    <p><strong>Nome</strong>
			                    </p>
			                    <p>
			                        ${empresa.nome}
			                    </p>
			                </div>
			                <div class="col-md-6">
			                    <p><strong>Razão Social</strong>
			                    </p>
			                    <p>
			                        ${empresa.razaoSocial}
			                    </p>
			                </div>
			                <div class="col-md-6">
			                    <p><strong>Telefone</strong>
			                    </p>
			                    <p>
			                        ${empresa.telefone}
			                    </p>
			                </div>
			                <div class="col-md-6">
			                    <p><strong>Email</strong>
			                    </p>
			                    <p>
			                        ${empresa.email}
			                    </p>
			                </div>
			            </div>
			            <hr />
			            
                    <div id="actions" class="row">
                        <div class="col-md-12">
                            <a class="btn btn-success btn-xs" href="ManterEmpresa.do?acao=Editar&id=${empresa.id }">Editar</a>
                            <button id="btn${empresa.id }%>" type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-modal" data-empresa="${empresa.id }">Excluir</button>
                            <a href="ListarEmpresas.jsp" class="btn btn-default">Voltar</a>
                        </div>
                    </div>
                </div>
                
                
                 <script src="js/jquery.min.js"></script>
            	 <script src="js/bootstrap.min.js"></script>
           		 <script type="text/javascript">
              		  $("#delete-modal").on('show.bs.modal', function(event) {
                    var button = $(event.relatedTarget); //botao que disparou a modal
                    var recipient = button.data('empresa');
                    $("#id_excluir").val(recipient);
                });
            </script>
             
        </body>

        </html>