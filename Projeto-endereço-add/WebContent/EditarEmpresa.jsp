<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

        <!DOCTYPE html>
        <html lang="pt-br">

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Alterar Empresa</title>

        </head>

        <body>
                <!-- Barra superior com os menus de navegação -->
				<c:import url="header-empresa.jsp"/>
                <!-- Container Principal -->
                <div id="main" class="container">
                    <h3 class="page-header">Alterar Empresa "${empresa.nome }"</h3>
                    <!-- Formulario para alteração de empresas -->
                    <form action= methods/atualizarEmp.jsp>
                        <!-- area de campos do form -->
                        <input type="hidden" name="id" value="${empresa.id }" />
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="nome">CNPJ</label>
                                <input type="text" class="form-control" name="cnpj" id="cnpj" required maxlength="100" placeholder="Insira um novo CNPJ para a Empresa" value="${empresa.cnpj }">
                            </div>
                        </div>
                        
                          <div class="row">
                            <div class="form-group col-md-6">
                                <label for="razaoSocial">Razão Social</label>
                                <input type="text" class="form-control" name="razaoSocial" id="razaoSocial" maxlength="15"  placeholder="Insira uma nova Razão Social para a Empresa" value="${empresa.razaoSocial }">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" name="nome" id="nome" required maxlength="60" placeholder="Insira um novo Nome para a Empresa" value="${empresa.nome }">
                            </div>
                            
                             <div class="form-group col-md-6">
                                <label for="telefone">Telefone</label>
                                <input type="text" class="form-control" name="telefone" id="telefone" required maxlength="60" placeholder="Insira um novo Telefone para a Empresa" value="${empresa.telefone }">
                            </div>
                            
        
                            
                             <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" id="email" required maxlength="60" placeholder="Insira um novo E-mail para a Empresa" value="${empresa.email }">
                            </div>
                        </div>
                        <hr />
                        <div id="actions" class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" name="acao" value="Alterar">Salvar</button>
                                <a href="ListarEmpresas.jsp" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
        </body>

        </html>