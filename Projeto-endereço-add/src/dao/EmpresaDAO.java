package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Empresa;




public class EmpresaDAO {
	

	

		//METODO C R U D
			
			public int criar(Empresa empresa) {
				String sqlInsert = "INSERT INTO empresa(cnpj, razaoSocial, nome, documento, telefone, email, numero, cep, logradouro, bairro, cidade, estado) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
					stm.setString(1, empresa.getCnpj());
					stm.setString(2, empresa.getRazaoSocial());
					stm.setString(3, empresa.getNome());
					stm.setString(4, empresa.getDocumento());
					stm.setInt(5, empresa.getTelefone());
					stm.setString(6, empresa.getEmail());
					stm.setInt(7, empresa.getNumero());
					stm.setInt(8, empresa.getCEP());
					stm.setString(9, empresa.getLogradouro());
					stm.setString(10, empresa.getBairro());
					stm.setString(11, empresa.getCidade());
					stm.setString(12, empresa.getEstado());
					stm.execute();
					String sqlQuery  = "SELECT LAST_INSERT_ID()";
					try(PreparedStatement stm2 = conn.prepareStatement(sqlQuery);
						ResultSet rs = stm2.executeQuery();) {
						if(rs.next()){
							empresa.setId(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return empresa.getId();
			}
			
			public void excluir(int id) {
				String sqlDelete = "DELETE FROM empresa WHERE id = ?";
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
					stm.setInt(1, id);
					stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void atualizar(Empresa empresa) {
				String sqlUpdate = "UPDATE empresa SET cnpj=?, razaoSocial=?, nome=?, telefone=?, email=?, numero=?,cep=?,logradouro=?,bairro=?,cidade=?,estado=? +  WHERE id=?";
				// usando o try with resources do Java 7, que fecha o que abriu
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlUpdate);) {
					stm.setString(1, empresa.getCnpj());
					//stm.setString(2, empresa.getSenha());
					//stm.setString(3, empresa.getSituacaoCad());
					stm.setString(2, empresa.getRazaoSocial());
					stm.setString(3, empresa.getNome());
					//stm.setString(6, empresa.getDocumento());
					stm.setInt(4, empresa.getTelefone());
					stm.setString(5, empresa.getEmail());
					stm.setInt(7, empresa.getNumero());
					stm.setInt(8, empresa.getCEP());
					stm.setString(9, empresa.getLogradouro());
					stm.setString(10, empresa.getBairro());
					stm.setString(11, empresa.getCidade());
					stm.setString(12, empresa.getEstado());
					
					stm.setInt(6, empresa.getId());
				stm.execute();
				} catch (Exception e) {
				e.printStackTrace();
				}
				}
			
			
			
			public Empresa carregar(int id) {
				
				Empresa empresa = new Empresa();
				empresa.setId(id);
				String sqlSelect = "SELECT cnpj, razaoSocial, nome, documento, telefone, email, numero, cep, logradouro, bairro, cidade, estado"
						+ "  FROM empresa WHERE Empresa.id = ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
					stm.setInt(1, empresa.getId());
					try (ResultSet rs = stm.executeQuery();) {
						if (rs.next()) {
							empresa.setCnpj(rs.getString("cnpj"));
							empresa.setRazaoSocial(rs.getString("razaoSocial"));
							empresa.setNome(rs.getString("nome"));
							empresa.setDocumento(rs.getString("documento"));
							empresa.setTelefone(rs.getInt("telefone"));
							empresa.setEmail(rs.getString("email"));
							empresa.setNumero(rs.getInt("numero"));
							empresa.setCEP(rs.getInt("cep"));
							empresa.setLogradouro(rs.getString("logradouro"));
							empresa.setBairro(rs.getString("bairro"));
							empresa.setCidade(rs.getString("cidade"));
							empresa.setEstado(rs.getString("estado"));

						} else {
							empresa.setId(-1);
							empresa.setNome(null);

							
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
			}
				return empresa;
			}
			
			
			public ArrayList<Empresa> listarEmpresas() {
				Empresa empresa;
				ArrayList<Empresa> lista = new ArrayList<>();
				String sqlSelect = "SELECT id, cnpj, razaoSocial, nome, telefone, email, numero, cep, logradouro, bairro, cidade, estado  FROM empresa";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
				try (ResultSet rs = stm.executeQuery();) {
				while (rs.next()) {
				empresa = new Empresa();
				empresa.setId(rs.getInt("id"));
				empresa.setCnpj(rs.getString("cnpj"));
				empresa.setRazaoSocial(rs.getString("razaoSocial"));
				empresa.setNome(rs.getString("nome"));
				empresa.setTelefone(rs.getInt("telefone"));
				empresa.setEmail(rs.getString("email"));
				empresa.setNumero(rs.getInt("numero"));
				empresa.setCEP(rs.getInt("cep"));
				empresa.setLogradouro(rs.getString("logradouro"));
				empresa.setBairro(rs.getString("bairro"));
				empresa.setCidade(rs.getString("cidade"));
				empresa.setEstado(rs.getString("estado"));
				
					if (rs.getString("email") != null) {
						
					
					lista.add(empresa);
					
					}
			
				}
				} catch (SQLException e) {
				e.printStackTrace();
				}
				} catch (SQLException e1) {
				System.out.print(e1.getStackTrace());
				}
				return lista;
				}
			
			
			public ArrayList<Empresa> listarEmpresas(String chave) {
				System.out.println("entrou");
				Empresa empresa;
				ArrayList<Empresa> lista = new ArrayList<>();
				String sqlSelect = "SELECT id, cnpj, razaoSocial, nome, telefone, email, numero, cep, logradouro, bairro, cidade, estado FROM empresa where upper(nome) like ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
				stm.setString(1, "%" + chave.toUpperCase() + "%");
				try (ResultSet rs = stm.executeQuery();) {
				while (rs.next()) {
					
					empresa = new Empresa();
					empresa.setId(rs.getInt("id"));
					empresa.setCnpj(rs.getString("cnpj"));
					empresa.setRazaoSocial(rs.getString("razaoSocial"));
					empresa.setNome(rs.getString("nome"));
					empresa.setTelefone(rs.getInt("telefone"));
					empresa.setEmail(rs.getString("email"));
					empresa.setNumero(rs.getInt("numero"));
					empresa.setCEP(rs.getInt("cep"));
					empresa.setLogradouro(rs.getString("logradouro"));
					empresa.setBairro(rs.getString("bairro"));
					empresa.setCidade(rs.getString("cidade"));
					empresa.setEstado(rs.getString("estado"));
					
					if (rs.getString("email") != null) {
						
					
					lista.add(empresa);
					
					}
				}
				} catch (SQLException e) {
				e.printStackTrace();
				}
				} catch (SQLException e1) {
				System.out.print(e1.getStackTrace());
				}
				return lista;
				}
			
			
		}	
		
			
			

