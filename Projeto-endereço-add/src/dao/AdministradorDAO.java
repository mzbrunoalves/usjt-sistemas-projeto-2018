package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Administrador;


public class AdministradorDAO {
	

	

		//METODO C R U D
			
			public int criar(Administrador administrador) {
				String sqlInsert = "INSERT INTO administrador(nome, documento,razaoSocial, site, dataAbertura,descEcoJur,numero,cep,logradouro,bairro,cidade,estado) "
						+ "VALUES (?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?, ?)";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
					stm.setString(1, administrador.getNome());
					stm.setString(2, administrador.getDocumento());
					stm.setString(3, administrador.getRazaoSocial());
					stm.setString(4, administrador.getSite());
					stm.setInt(5, administrador.getDataAbertura());
					stm.setString(6, administrador.getDescEcoJur());
					stm.setInt(7, administrador.getNumero());
					stm.setInt(8, administrador.getCEP());
					stm.setString(9, administrador.getLogradouro());
					stm.setString(10, administrador.getBairro());
					stm.setString(11, administrador.getCidade());
					stm.setString(12, administrador.getEstado());
					stm.execute();
					String sqlQuery  = "SELECT LAST_INSERT_ID()";
					try(PreparedStatement stm2 = conn.prepareStatement(sqlQuery);
						ResultSet rs = stm2.executeQuery();) {
						if(rs.next()){
							administrador.setId(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return administrador.getId();
			}
			
			public void excluir(int id) {
				String sqlDelete = "DELETE FROM administrador WHERE id = ?";
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
					stm.setInt(1, id);
					stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void atualizar(Administrador administrador) {
				String sqlUpdate = "UPDATE administrador SET nome=?, documento=?,razaoSocial=?, site=?, dataAbertura=?,descEcoJur=?, numero=?, cep=?, logradouro=?, bairro=?, cidade=?, estado=? "
						+ " WHERE id=?";
				// usando o try with resources do Java 7, que fecha o que abriu
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlUpdate);) {
					stm.setString(1, administrador.getNome());
					stm.setString(2, administrador.getDocumento());
					stm.setString(3, administrador.getRazaoSocial());
					stm.setString(4, administrador.getSite());
					stm.setInt(5, administrador.getDataAbertura());
					stm.setString(6, administrador.getDescEcoJur());
					stm.setInt(7, administrador.getNumero());
					stm.setInt(8, administrador.getCEP());
					stm.setString(9, administrador.getLogradouro());
					stm.setString(10, administrador.getBairro());
					stm.setString(11, administrador.getCidade());
					stm.setString(12, administrador.getEstado());

					stm.setInt(4, administrador.getId());
				stm.execute();
				} catch (Exception e) {
				e.printStackTrace();
				}
				}
			
			
			
			public Administrador carregar(Administrador administrador) {
				String sqlSelect = "SELECT nome, documento,razaoSocial, site, dataAbertura,descEcoJur,email, numero, cep, logradouro, bairro, cidade, estado"
						+ "  FROM Administrador WHERE Administrador.id = ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
					stm.setInt(1, administrador.getId());
					try (ResultSet rs = stm.executeQuery();) {
						if (rs.next()) {
							administrador.setNome(rs.getString("nome"));
							administrador.setDocumento(rs.getString("documento"));
							administrador.setRazaoSocial(rs.getString("razaoSocial"));
							administrador.setSite(rs.getString("Site"));
							administrador.setDataAbertura(rs.getInt(213123127));
							administrador.setDescEcoJur(rs.getString("descEcoJur"));
							administrador.setEmail(rs.getString("email"));
							administrador.setNumero(rs.getInt("numero"));
							administrador.setCEP(rs.getInt("cep"));
							administrador.setLogradouro(rs.getString("logradouro"));
							administrador.setBairro(rs.getString("bairro"));
							administrador.setCidade(rs.getString("cidade"));
							administrador.setEstado(rs.getString("estado"));
							
						} else {
							administrador.setId(-1);
							administrador.setNome(null);

							
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
			}
				return administrador;
			}
			
			
			
		}	
		
			
			

