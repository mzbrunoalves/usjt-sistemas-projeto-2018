	package model;

import java.io.Serializable;

public class Cartorio implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private int id;
		private String cnpj;
		private String nome;
		private String documento;
		private int telefone;
		private int situacaoCad;
		private int numero;
		private int CEP;
		private String logradouro;
		private String bairro;
		private String cidade;
		private String estado;
		
		//n�o obrigatorios
		private String horarioFunc;
		private String site;
		

		
		public Cartorio() {
		}
		
		
		
		public int getId() {
			return id;
		}



		public void setId(int id) {
			this.id = id;
		}



		public String getCnpj() {
			return cnpj;
		}



		public void setCnpj(String cnpj) {
			this.cnpj = cnpj;
		}
		
		
		
		public String getNome() {
			return nome;
		}



		public void setNome(String nome) {
			this.nome = nome;
		}


		
		public int getTelefone() {
			return telefone;
		}



		public void setTelefone(int telefone) {
			this.telefone = telefone;
		}

		
		
		public String getHorarioFunc() {
			return horarioFunc;
		}

		
		
		public void setHorarioFunc(String horarioFunc) {
			this.horarioFunc = horarioFunc;
		}

		
		
		public String getSite() {
			return site;
		}

		
		
		public void setSite(String site) {
			this.site = site;
		}


		@Override
		public String toString() {
			return "Id= " + id + ", \nC.N.P.J= " + cnpj +  "\nNome= " + nome 
					+ "\nTelefone= " + telefone + horarioFunc +  "\nHorario de funcionamento= "
					+ site +  "\nSite= " + "\n-------------";
		}



		public String getDocumento() {
			return documento;
		}



		public void setDocumento(String documento) {
			this.documento = documento;
		}



		public int getSituacaoCad() {
			return situacaoCad;
		}



		public void setSituacaoCad(int situacaoCad) {
			this.situacaoCad = situacaoCad;
		}



		public int getNumero() {
			return numero;
		}



		public void setNumero(int numero) {
			this.numero = numero;
		}



		public int getCEP() {
			return CEP;
		}



		public void setCEP(int cEP) {
			CEP = cEP;
		}



		public String getLogradouro() {
			return logradouro;
		}



		public void setLogradouro(String logradouro) {
			this.logradouro = logradouro;
		}



		public String getBairro() {
			return bairro;
		}



		public void setBairro(String bairro) {
			this.bairro = bairro;
		}



		public String getCidade() {
			return cidade;
		}



		public void setCidade(String cidade) {
			this.cidade = cidade;
		}



		public String getEstado() {
			return estado;
		}



		public void setEstado(String estado) {
			this.estado = estado;
		}


		
		
			


		
		
		
		


		
		





		
}	