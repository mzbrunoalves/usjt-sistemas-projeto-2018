package model;

import java.io.Serializable;

public class Administrador implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private int id;
		private String nome;
		private String documento;
		private String razaoSocial;
		private String email;
		private int numero;
		private int CEP;
		private String logradouro;
		private String bairro;
		private String cidade;
		private String estado;
		
		public int getNumero() {
			return numero;
		}


		public void setNumero(int numero) {
			this.numero = numero;
		}


		public int getCEP() {
			return CEP;
		}


		public void setCEP(int cEP) {
			CEP = cEP;
		}


		public String getLogradouro() {
			return logradouro;
		}


		public void setLogradouro(String logradouro) {
			this.logradouro = logradouro;
		}


		public String getBairro() {
			return bairro;
		}


		public void setBairro(String bairro) {
			this.bairro = bairro;
		}


		public String getCidade() {
			return cidade;
		}


		public void setCidade(String cidade) {
			this.cidade = cidade;
		}


		public String getEstado() {
			return estado;
		}


		public void setEstado(String estado) {
			this.estado = estado;
		}




		//n�o obrigatorios
		private String site;
		private int dataAbertura;
		private String descEcoJur;
		
		public Administrador() {
		}
	
	
		public int getId() {
			return id;
		}


		
		public void setId(int id) {
			this.id = id;
		}



		public String getNome() {
			return nome;
		}


		
		public void setNome(String nome) {
			this.nome = nome;
		}



		public String getDocumento() {
			return documento;
		}



		public void setDocumento(String documento) {
			this.documento = documento;
		}



		public String getRazaoSocial() {
			return razaoSocial;
		}

		

		public void setRazaoSocial(String razaoSocial) {
			this.razaoSocial = razaoSocial;
		}



		public String getEmail() {
			return email;
		}



		public void setEmail(String email) {
			this.email = email;
		}



		public String getSite() {
			return site;
		}



		public void setSite(String site) {
			this.site = site;
		}



		public int getDataAbertura() {
			return dataAbertura;
		}



		public void setDataAbertura(int dataAbertura) {
			this.dataAbertura = dataAbertura;
		}



		public String getDescEcoJur() {
			return descEcoJur;
		}



		public void setDescEcoJur(String descEcoJur) {
			this.descEcoJur = descEcoJur;
		}




		@Override
		public String toString() {
			return "Id= " + id + "\n Nome= " + nome + "\n Documento= " + documento 
					+ "\n Razao Social= " + razaoSocial + "\n E-mail= " + email 
					+ "\n Site= " + site + "\n Data de Abertura= " + dataAbertura 
					+ "\n Descri��o Economica / Juridica= " + descEcoJur  +"\n-------------";
		}
		

			


		
		
		
		


		
		





		
}	