package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Administrador;
import service.AdministradorService;


/**
 * Servlet implementation class ManterPaisController
 */
@WebServlet("/ManterAdministrador.do")
public class ManterAdministradorController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 private int id;
*/	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome = request.getParameter("nome");
		String documento = request.getParameter("documento");
		String razaoSocial = request.getParameter("razaoSocial");
		String site = request.getParameter("site");
		int dataAbertura = Integer.parseInt(request.getParameter("dataAbertura"));
		String descEcoJur = request.getParameter("descEcoJur");
		int numero = Integer.parseInt(request.getParameter("numero"));
		int cep = Integer.parseInt(request.getParameter("cep"));
		String logradouro = request.getParameter("logradouro");
		String bairro = request.getParameter("estado");
		String cidade = request.getParameter("cidade");
		String estado = request.getParameter("estado");

		
		//instanciar o javabean
		Administrador administrador = new Administrador();
		administrador.setNome(nome);
		administrador.setDocumento(documento);
		administrador.setRazaoSocial(razaoSocial);
		administrador.setSite(site);
		administrador.setDataAbertura(dataAbertura);
		administrador.setDescEcoJur(descEcoJur);
		administrador.setNumero(numero);
		administrador.setCEP(cep);
		administrador.setLogradouro(logradouro);
		administrador.setBairro(bairro);
		administrador.setCidade(cidade);
		administrador.setEstado(estado);
		
		//instanciar o service
		AdministradorService adm = new AdministradorService();
		adm.criar(administrador);
		administrador = adm.carregar(administrador);


	}
	



}
