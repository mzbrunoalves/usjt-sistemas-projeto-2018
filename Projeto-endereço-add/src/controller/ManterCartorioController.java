package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Cartorio;
import service.CartorioService;


/**
 * Servlet implementation class ManterPaisController
 */
@WebServlet("/ManterCartorio.do")
public class ManterCartorioController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 private int id;
*/	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cnpj = request.getParameter("cnpj");
		String nome = request.getParameter("nome");
		int telefone = Integer.parseInt(request.getParameter("telefone"));
		String horarioFunc = request.getParameter("horarioFunc");
		String site = request.getParameter("site");
		int numero = Integer.parseInt(request.getParameter("numero"));
		int cep = Integer.parseInt(request.getParameter("cep"));
		String logradouro = request.getParameter("logradouro");
		String bairro = request.getParameter("estado");
		String cidade = request.getParameter("cidade");
		String estado = request.getParameter("estado");
		
		//instanciar o javabean
		Cartorio cartorio = new Cartorio();
		cartorio.setCnpj(cnpj);
		cartorio.setNome(nome);
		cartorio.setTelefone(telefone);
		cartorio.setHorarioFunc(horarioFunc);
		cartorio.setSite(site);
		cartorio.setNumero(numero);
		cartorio.setCEP(cep);
		cartorio.setLogradouro(logradouro);
		cartorio.setBairro(bairro);
		cartorio.setCidade(cidade);
		cartorio.setEstado(estado);
		
		CartorioService cart = new CartorioService();
		cart.criar(cartorio);
		cartorio = cart.carregar(cartorio);

	}
}
