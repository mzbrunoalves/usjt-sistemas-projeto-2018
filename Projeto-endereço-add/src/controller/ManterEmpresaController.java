package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Empresa;

import service.EmpresaService;


/**
 * Servlet implementation class ManterPaisController
 */
@WebServlet("/ManterEmpresa.do")
public class ManterEmpresaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pAcao = request.getParameter("acao");
		String pId = request.getParameter("id");
		String Ecnpj = request.getParameter("cnpj");
		String ErazaoSocial = request.getParameter("razaoSocial");
		String Enome = request.getParameter("nome");
		String Edocumento = request.getParameter("documento");
		String Etelefone = request.getParameter("telefone");
		String Eemail = request.getParameter("email");
		int numero = Integer.parseInt(request.getParameter("numero"));
		int cep = Integer.parseInt(request.getParameter("cep"));
		String logradouro = request.getParameter("logradouro");
		String bairro = request.getParameter("estado");
		String cidade = request.getParameter("cidade");
		String estado = request.getParameter("estado");
		
		int id = 0;
		int tel = 1;
		
		try {
			System.out.println("entrou na convers�o");
			id = Integer.parseInt(pId);
			tel = Integer.parseInt(Etelefone);
			} catch (NumberFormatException e) {
			}
		
		//instanciar o javabean
		Empresa empresa = new Empresa();
		empresa.setId(id);
		empresa.setCnpj(Ecnpj);
		empresa.setRazaoSocial(ErazaoSocial);
		empresa.setNome(Enome);
		//empresa.setDocumento(Edocumento);
		empresa.setTelefone(tel);
		empresa.setEmail(Eemail);
		empresa.setNumero(numero);
		empresa.setCEP(cep);
		empresa.setLogradouro(logradouro);
		empresa.setBairro(bairro);
		empresa.setCidade(cidade);
		empresa.setEstado(estado);
		
		//instanciar o service
		EmpresaService es = new EmpresaService();
		RequestDispatcher view = null;
		HttpSession session = request.getSession();
		
		
		if (pAcao.equals("Excluir")) {
			System.out.println("entrou no excluir");
			es.excluir(empresa.getId());
			ArrayList<Empresa> lista = (ArrayList<Empresa>)session.getAttribute( "lista" ) ;
			lista.remove(busca(empresa, lista));
			session.setAttribute("lista", lista);
			view = request.getRequestDispatcher("ListarEmpresas.jsp");
			} 
			else if (pAcao.equals("Alterar")) {
			es.atualizar(empresa);
			ArrayList<Empresa> lista = (ArrayList<Empresa>) session .getAttribute( "lista" ) ;
			int pos = busca(empresa, lista);
			lista.remove(pos);
			lista.add(pos, empresa);
			session.setAttribute("lista", lista);
			request.setAttribute("empresa", empresa);
			view = request.getRequestDispatcher("VisualizarEmpresa.jsp");
			} 
			else if (pAcao.equals("Visualizar")) {
			empresa = es.carregar(empresa.getId());
			request.setAttribute("empresa", empresa);
			view = request.getRequestDispatcher("VisualizarEmpresa.jsp");
			} 
			else if (pAcao.equals("Editar")) {
			empresa = es.carregar(empresa.getId());
			request.setAttribute("empresa", empresa);
			view = request.getRequestDispatcher("EditarEmpresa.jsp");
			}
			view.forward(request, response);
			}
	
	
	public int busca(Empresa empresa, ArrayList<Empresa> lista) {
		Empresa to;
		System.out.println("");
		for(int i = 0; i < lista.size(); i++){
		to = lista.get(i);
		if(to.getId() == empresa.getId()){
		return i;
		}
		}
		return -1;
		}

	}
	




