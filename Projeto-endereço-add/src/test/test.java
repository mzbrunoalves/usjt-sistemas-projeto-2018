package test;

import static org.junit.Assert.assertEquals;
import model.Empresa;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import service.EmpresaService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class test {
	Empresa empresa, copia;
	EmpresaService empresaService;
	static int id = 0;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("____Setup____");
		
		empresa = new Empresa();
		empresa.setId(id);
		empresa.setCnpj("xxxxxxxxx");
		empresa.setRazaoSocial("Shitpost Industries LTDA");
		empresa.setNome("Rhuan Carlitos");
		empresa.setDocumento("xxxx");
		empresa.setTelefone(989898951);
		empresa.setEmail("rhuan_adm@shitpostindustries.com");
		
		copia = new Empresa();
		empresa.setId(id);
		empresa.setCnpj("xxxxxxxxx");
		empresa.setRazaoSocial("Shitpost Industries LTDA");
		empresa.setNome("Rhuan Carlitos");
		empresa.setDocumento("xxxx");
		empresa.setTelefone(989898951);
		empresa.setEmail("rhuan_adm@shitpostindustries.com");
		
		empresaService = new EmpresaService();

		System.out.println(empresa);
		System.out.println(copia);
		System.out.println(id);
		
	}
	
	@Test
	public void test00Carregar() {
		System.out.println("Carregar \n");
		//para funcionar o cliente 1 deve ter sido carregado no banco por fora
		Empresa fixture = new Empresa();

		fixture.setId(-1);
		fixture.setNome("Carlos Drummond de Andrade");
		empresa.setDocumento("339399393");

		
		EmpresaService novoService = new EmpresaService();
		//Empresa novo = novoService.carregar(empresa);
		//assertEquals("testa inclusao", novo.getId(), fixture.getId());
	}
	

	@Test
	public void test01Criar() {
		System.out.println("Criar" + "\n-------------");
		id = empresaService.criar(empresa);
		System.out.println(id);
		copia.setId(id);
		assertEquals("testa criacao", empresa.getId(), copia.getId());
	}
	
	/*@Test
	public void test02Atualizar() {
		System.out.println("Atualizar" + "\n-------------");
		empresa.setNome("Tanto faz");
		copia.setNome("Tanto faz");		
		empresaService.atualizar(empresa);
		empresa = empresaService.carregar(empresa);
		assertEquals("testa atualizacao", empresa.getNome(), copia.getNome());
	}
	
	@Test
	public void test03Excluir() {
		System.out.println("Excluir" + "\n-------------");

		copia.setId(-1);
		copia.setNome(null);

		
		empresaService.excluir(id);
		empresa = empresaService.carregar(empresa);
		
		assertEquals("testa exclusao", empresa.getId(), copia.getId());
	}
	*/



}