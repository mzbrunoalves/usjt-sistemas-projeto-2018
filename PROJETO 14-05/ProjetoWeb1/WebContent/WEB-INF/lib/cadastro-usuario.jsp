<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport">
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Animate -->
<link rel="stylesheet" href="css/animate.css">

<!-- Main Style -->
<link rel="stylesheet" href="style.css">
<title>Power Soft - Cadastro de usuário</title>
</head>
<body class="cadastrar-empresa">
	<!-- <header id="lateral-menu-bar">
			<nav>
				<ul class="menu">
					<li class="ativo">
						<a href="#"><img src="images/icon-add.png" alt="Adicionar"><span>Adicionar empresa</span></a></li>
					<li><a href="listar-empresas.html"><img src="images/icon-menu.png" alt="Listar"><span>Listar empresas</span></a></li>
				</ul>
			</nav>
		</header> -->
	<div class="">
		<header id="top-nav">
			<div class="row">
				<div class="col-6"></div>
				<div class="col-6 pull-right">
					<div class="informations">
						<!-- <img src="images/user.png" alt=""> -->
						<a href="login.jsp">Voltar</a>
					</div>
				</div>
			</div>
		</header>
		<main>
		<div class="row">
			<div class="col-lg-4 col-centered box">
				<h1>Cadastro de usuário</h1>
				<h3>Dados de acesso</h3>
				<form action="methods/registro.jsp">
					<select name="tipo-usuario" class="col-lg-4" id="">
						<option value="empresa">Empresa</option>
						<option value="cartorio">Cartório</option>
					</select> <input type="text" name="usuario" required="required"
						class="col-lg-6" placeholder="E-mail"> <input
						type="password" name="senha" required="required" class="col-lg-12"
						placeholder="Senha">
					<!-- <input type="password" value="" required="required" class="col-lg-12" placeholder="Repita a senha"> -->
					<button type="submit" class="col-lg-3">Cadastrar</button>
				</form>
			</div>
		</div>
	</div>
	</main>
	<!-- Importando o Footer -->
	<c:import url="footer.jsp" />