package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Empresa;
import service.EmpresaService;


/**
 * Servlet implementation class ManterPaisController
 */
@WebServlet("/ManterEmpresa.do")
public class ManterEmpresaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 private int id;
*/	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Ecnpj = request.getParameter("cnpj");
		String ErazaoSocial = request.getParameter("razaoSocial");
		String Enome = request.getParameter("nome");
		String Edocumento = request.getParameter("documento");
		int Etelefone = Integer.parseInt(request.getParameter("telefone"));
		String Eemail = request.getParameter("email");
		
		//instanciar o javabean
		Empresa empresa = new Empresa();
		empresa.setCnpj(Ecnpj);
		empresa.setRazaoSocial(ErazaoSocial);
		empresa.setNome(Enome);
		empresa.setDocumento(Edocumento);
		empresa.setTelefone(Etelefone);
		empresa.setEmail(Eemail);

		
		//instanciar o service
		EmpresaService emp = new EmpresaService();
		emp.criar(empresa);
		empresa = emp.carregar(empresa);


	}
	



}
