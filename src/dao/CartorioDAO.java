package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Cartorio;


public class CartorioDAO {
	

	

		//METODO C R U D
			
			public int criar(Cartorio cartorio) {
				String sqlInsert = "INSERT INTO cartorio(cnpj, nome, telefone, horarioFunc, site) "
						+ "VALUES (?, ?, ?, ?, ?)";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
					stm.setString(1, cartorio.getCnpj());
					stm.setString(2, cartorio.getNome());
					stm.setInt(3, cartorio.getTelefone());
					stm.setString(4, cartorio.getHorarioFunc());
					stm.setString(5, cartorio.getSite());
					stm.execute();
					String sqlQuery  = "SELECT LAST_INSERT_ID()";
					try(PreparedStatement stm2 = conn.prepareStatement(sqlQuery);
						ResultSet rs = stm2.executeQuery();) {
						if(rs.next()){
							cartorio.setId(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return cartorio.getId();
			}
			
			public void excluir(int id) {
				String sqlDelete = "DELETE FROM cartorio WHERE id = ?";
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
					stm.setInt(1, id);
					stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void atualizar(Cartorio cartorio) {
				String sqlUpdate = "UPDATE cartorio SET cnpj=?, nome=?, telefone=?, horarioFunc=?, site=? "
						+ " WHERE id=?";
				// usando o try with resources do Java 7, que fecha o que abriu
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlUpdate);) {
					stm.setString(1, cartorio.getCnpj());
					stm.setString(2, cartorio.getNome());
					stm.setInt(3, cartorio.getTelefone());
					stm.setString(4, cartorio.getHorarioFunc());
					stm.setString(5, cartorio.getSite());

					stm.setInt(4, cartorio.getId());
				stm.execute();
				} catch (Exception e) {
				e.printStackTrace();
				}
				}
			
			
			
			public Cartorio carregar(Cartorio cartorio) {
				String sqlSelect = "SELECT cnpj, nome, telefone, horarioFunc, site"
						+ "  FROM cartorio WHERE Cartorio.id = ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
					stm.setInt(1, cartorio.getId());
					try (ResultSet rs = stm.executeQuery();) {
						if (rs.next()) {
							cartorio.setCnpj(rs.getString("nome"));
							cartorio.setNome(rs.getString("nome"));
							cartorio.setTelefone(rs.getInt("nome"));
							cartorio.setHorarioFunc(rs.getString("nome"));
							cartorio.setSite(rs.getString("nome"));
					

						} else {
							cartorio.setId(-1);
							cartorio.setNome(null);

							
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
			}
				return cartorio;
			}
			
			
			
		}	
		
			
			

