package model;

import java.io.Serializable;

public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private int id;
		private String documento;
		private String senha;
		private String situacaoCad;
		
		public Usuario() {
		}
		
		public int getId() {
			return id;
		}



		public void setId(int id) {
			this.id = id;
		}



		public String getDocumento() {
			return documento;
		}



		public void setDocumento(String documento) {
			this.documento = documento;
		}



		public String getSenha() {
			return senha;
		}



		public void setSenha(String senha) {
			this.senha = senha;
		}



		public String getSituacaoCad() {
			return situacaoCad;
		}



		public void setSituacaoCad(String situacaoCad) {
			this.situacaoCad = situacaoCad;
		}


		@Override
		public String toString() {
			return "Id= " + id + "Documento= " + documento +  "Senha= " + senha +  "Situa��o do cadastro= " + situacaoCad + "\n-------------";
		}
}	