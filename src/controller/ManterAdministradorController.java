package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Administrador;
import service.AdministradorService;


/**
 * Servlet implementation class ManterPaisController
 */
@WebServlet("/ManterAdministrador.do")
public class ManterAdministradorController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 private int id;
*/	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome = request.getParameter("nome");
		String documento = request.getParameter("documento");
		String razaoSocial = request.getParameter("razaoSocial");
		String site = request.getParameter("site");
		int dataAbertura = Integer.parseInt(request.getParameter("dataAbertura"));
		String descEcoJur = request.getParameter("descEcoJur");

		
		//instanciar o javabean
		Administrador administrador = new Administrador();
		administrador.setNome(nome);
		administrador.setDocumento(documento);
		administrador.setRazaoSocial(razaoSocial);
		administrador.setSite(site);
		administrador.setDataAbertura(dataAbertura);
		administrador.setDescEcoJur(descEcoJur);
		
		//instanciar o service
		AdministradorService adm = new AdministradorService();
		adm.criar(administrador);
		administrador = adm.carregar(administrador);


	}
	



}
