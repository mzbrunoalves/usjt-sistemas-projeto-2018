package controller;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import model.Usuario;
import service.UsuarioService;


@WebServlet("/ManterUsuario.do")
public class ManterUsuarioController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 private int id;
	 */	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String documento = request.getParameter("documento");
		String senha = request.getParameter("senha");
		String situacaoCad = request.getParameter("situacaoCad");

		
		//instanciar o javabean
		Usuario usuario = new Usuario();
		usuario.setDocumento(documento);
		usuario.setSenha(senha);
		usuario.setSituacaoCad(situacaoCad);

		
		//instanciar o service
		UsuarioService usu = new UsuarioService();
		usu.criar(usuario);
		usuario = usu.carregar(usuario);
		
		String un=request.getParameter("documento");
		String pw=request.getParameter("senha");
		
		// Connect to mysql and verify username password
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		 // loads driver
		Connection c = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/mydb?user=root&password=rhuto123"); // gets a new connection
 
		PreparedStatement ps = (PreparedStatement) c.prepareStatement("select documento, senha from usuario where documento=? and senha=?");
		ps.setString(1, un);
		ps.setString(2, pw);
 
		ResultSet rs = ps.executeQuery();
 
		while (rs.next()) {
		
			response.sendRedirect("../../home-empresa.jsp");
			return;
		}
		response.sendRedirect("error.html");
		return;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

		
		
	}
	




